from hashutils import HashUtils


class TestHashUtils:

    def test_hash_string_as_hexadecimal(self):

        o_hashutils = HashUtils()

        b_success, s_md5, s_last_chars = o_hashutils.hash_string_as_hexadecimal("Hello World", 2)
        assert b_success is True
        assert s_md5 == "b10a8db164e0754105b7a99be72e3fe5"
        assert s_last_chars == "e5"

        b_success, s_md5, s_last_chars = o_hashutils.hash_string_as_hexadecimal("Hello World", 4)
        assert b_success is True
        assert s_md5 == "b10a8db164e0754105b7a99be72e3fe5"
        assert s_last_chars == "3fe5"
