#
# Tests for StringUtils class
#
# Author: Carles Mateo
# Creation Date: 2014-01-01
# Last Update: 2021-10-13
#

import pytest
from subprocessutils import SubProcessUtils


class TestSubProcessUtils(object):

    def test_execute_command_for_output(self):

        o_subprocess = SubProcessUtils()

        i_code, s_stdout, s_stderr = o_subprocess.execute_command_for_output("python3 --version", b_shell=True, b_convert_to_ascii=True, b_convert_to_utf8=False)

        assert i_code == 0
        assert s_stderr == ""
        assert s_stdout.find("Python 3.") == 0

        i_code, s_stdout, s_stderr = o_subprocess.execute_command_for_output("python3 --version", b_shell=True, b_convert_to_ascii=False, b_convert_to_utf8=True)

        assert i_code == 0
        assert s_stderr == ""
        assert s_stdout.find("Python 3.") == 0

    def test_execute_command_for_output_fail(self):

        o_subprocess = SubProcessUtils()

        i_code, s_stdout, s_stderr = o_subprocess.execute_command_for_output("does-not-exist-should-fail", b_shell=True, b_convert_to_ascii=True, b_convert_to_utf8=False)

        assert i_code > 0
        assert s_stderr != ""
        assert s_stdout == ""

    def test_text_execute_command_with_no_pipe_and_no_stderr(self):

        o_subprocess = SubProcessUtils()

        s_command = "python3 --version"

        s_stdout = o_subprocess.execute_command_with_no_pipe_and_no_stderr(s_command, b_shell=True)
        assert s_stdout.find("Python 3.") == 0
