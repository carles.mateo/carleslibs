import pytest
import version

class TestVersion:

    def test_version(self):
        assert version.s_version == "1.0.8"
        assert version.VERSION == "1.0.8"
