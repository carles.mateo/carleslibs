#
# Tests for DateTimeUtils class
#
# Author: Carles Mateo
# Creation Date: 2019-11-20 17:38 IST
# Last Update:   2020-08-16 18:50 IST
#

import pytest
import time
from src.datetimeutils import DateTimeUtils


class TestDateTimeUtils(object):

    # Start Tests
    def test_get_datetime(self):
        o_datetime = DateTimeUtils()

        s_datetime = o_datetime.get_datetime(b_milliseconds=False)

        assert s_datetime != ""
        assert len(s_datetime) == 19
        assert s_datetime[4] == "-"
        assert s_datetime[7] == "-"
        assert s_datetime[10] == " "
        assert s_datetime[13] == ":"
        assert s_datetime[16] == ":"

        # The Data has to be more recent than now
        assert s_datetime > "2020-08-16 10:10:10"

    def test_get_datetime_with_milliseconds(self):
        o_datetime = DateTimeUtils()

        s_datetime = o_datetime.get_datetime(b_milliseconds=True)

        assert s_datetime != ""
        assert len(s_datetime) > 19
        assert s_datetime[4] == "-"
        assert s_datetime[7] == "-"
        assert s_datetime[10] == " "
        assert s_datetime[13] == ":"
        assert s_datetime[16] == ":"

        assert s_datetime[19] == "."
        # The Data has to be more recent than now
        assert s_datetime > "2020-08-16 10:10:10"

    def test_get_datetime_without_spaces(self):
        o_datetime = DateTimeUtils()

        s_datetime = o_datetime.get_datetime(b_milliseconds=False, b_remove_spaces_and_colons=True)

        assert s_datetime != ""
        assert len(s_datetime) == 17
        assert s_datetime[4] == "-"
        assert s_datetime[7] == "-"
        assert s_datetime[10] == "-"

        # The Data has to be more recent than now
        assert s_datetime > "2020-08-16-101010"

    def test_get_datetime_without_spaces_and_dashes(self):
        o_datetime = DateTimeUtils()

        s_datetime = o_datetime.get_datetime(b_milliseconds=False, b_remove_spaces_and_colons=True, b_remove_dashes=True)

        assert s_datetime != ""
        assert len(s_datetime) == 14

        # The Data has to be more recent than now
        assert s_datetime > "20200816101010"

    def test_get_datetime_as_14_string(self):
        o_datetime = DateTimeUtils()

        s_datetime = o_datetime.get_datetime_as_14_string()

        assert s_datetime != ""
        assert len(s_datetime) == 14

        # The Data has to be more recent than now
        assert s_datetime > "20200816101010"


    def test_get_unix_epoch(self):
        o_datetime = DateTimeUtils()

        s_epoch = o_datetime.get_unix_epoch()
        assert s_epoch != ""
        assert len(s_epoch) >= 10
        assert int(s_epoch) > 1591210336

    def test_get_unix_epoch_as_int(self):
        o_datetime = DateTimeUtils()

        i_epoch = o_datetime.get_unix_epoch_as_int()
        assert i_epoch > 0
        assert isinstance(i_epoch, int)
        assert i_epoch > 1591210336

    def test_get_unix_epoch_as_float(self):
        o_datetime = DateTimeUtils()

        f_datetime = o_datetime.get_unix_epoch_as_float()

        assert isinstance(f_datetime, float)
        assert f_datetime > 1591210336

    def test_sleep(self):
        o_datetime = DateTimeUtils()

        i_now_epoch = int(time.time())
        o_datetime.sleep(1)
        i_future_epoch = int(time.time())

        assert i_future_epoch >= i_now_epoch + 1
