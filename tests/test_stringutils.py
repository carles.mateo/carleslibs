#
# Tests for StringUtils class
#
# Author: Carles Mateo
# Creation Date: 2014-01-01
# Last Update: 2021-05-22
#

import pytest
from stringutils import StringUtils


class TestStringUtils(object):

    def test_convert_integer_to_string_thousands(self):

        o_stringutils = StringUtils()

        s_number_formatted = o_stringutils.convert_integer_to_string_thousands(i_number=1000, s_thousand_sep=",")

        assert s_number_formatted == "1,000"

        s_number_formatted = o_stringutils.convert_integer_to_string_thousands(i_number=1000000, s_thousand_sep=",")

        assert s_number_formatted == "1,000,000"

        s_number_formatted = o_stringutils.convert_integer_to_string_thousands(i_number=-100, s_thousand_sep=",")

        assert s_number_formatted == "-100"

        s_number_formatted = o_stringutils.convert_integer_to_string_thousands(i_number=-1000000, s_thousand_sep=",")

        assert s_number_formatted == "-1,000,000"

    def test_convert_string_to_integer_ok(self):
        o_stringutils = StringUtils()

        b_success, i_number = o_stringutils.convert_string_to_integer("1000")
        assert b_success is True
        assert i_number == 1000

    def test_convert_string_to_integer_value_empty_string(self):
        o_stringutils = StringUtils()

        b_success, i_number = o_stringutils.convert_string_to_integer("")
        assert b_success is True
        assert i_number == 0

    def test_convert_string_to_integer_ko(self):
        o_stringutils = StringUtils()

        b_success, i_number = o_stringutils.convert_string_to_integer("1000a")
        assert b_success is False
        assert i_number == 0

    def test_convert_to_gb_from_kb_ok_with_units(self):
        o_stringutils = StringUtils()

        b_success, s_amount_gb, f_amount_gb = o_stringutils.convert_to_gb_from_kb("100KB", b_add_units=True)
        assert b_success is True
        assert s_amount_gb == "0GB"
        assert f_amount_gb == 0

        b_success, s_amount_gb, f_amount_gb = o_stringutils.convert_to_gb_from_kb("100kb", b_add_units=True)
        assert b_success is True
        assert s_amount_gb == "0GB"
        assert f_amount_gb == 0

        b_success, s_amount_gb, f_amount_gb = o_stringutils.convert_to_gb_from_kb("0", b_add_units=True)
        assert b_success is False
        assert s_amount_gb == "0GB"
        assert f_amount_gb == 0

        b_success, s_amount_gb, f_amount_gb = o_stringutils.convert_to_gb_from_kb("1048576KB", b_add_units=True)
        assert b_success is True
        assert s_amount_gb == "1GB"
        assert f_amount_gb == 1

        b_success, s_amount_gb, f_amount_gb = o_stringutils.convert_to_gb_from_kb("1048576K", b_add_units=True)
        assert b_success is True
        assert s_amount_gb == "1GB"
        assert f_amount_gb == 1

        b_success, s_amount_gb, f_amount_gb = o_stringutils.convert_to_gb_from_kb("1150976K", b_add_units=True)
        assert b_success is True
        assert s_amount_gb == "1.10GB"
        assert f_amount_gb == 1.1

        b_success, s_amount_gb, f_amount_gb = o_stringutils.convert_to_gb_from_kb("", b_add_units=True)
        assert b_success is False
        assert s_amount_gb == "0GB"
        assert f_amount_gb == 0

    def test_convert_to_gb_from_kb_ok_without_units(self):
        o_stringutils = StringUtils()

        b_success, s_amount_gb, f_amount_gb = o_stringutils.convert_to_gb_from_kb("1150976K", b_add_units=False)

        assert b_success is True
        assert s_amount_gb == "1.10"
        assert f_amount_gb == 1.1

        b_success, s_amount_gb, f_amount_gb = o_stringutils.convert_to_gb_from_kb("", b_add_units=False)

        assert b_success is False
        assert s_amount_gb == "0"
        assert f_amount_gb == 0

    def test_convert_to_gb_from_kb_ko_without_units(self):
        o_stringutils = StringUtils()

        b_success, s_amount_gb, f_amount_gb = o_stringutils.convert_to_gb_from_kb("1150976XB", b_add_units=False)

        assert b_success is False
        assert s_amount_gb == "1150976XB"
        assert f_amount_gb == 0

        b_success, s_amount_gb, f_amount_gb = o_stringutils.convert_to_gb_from_kb("XXX", b_add_units=False)

        assert b_success is False
        assert s_amount_gb == "XXX"
        assert f_amount_gb == 0

    def test_convert_bytes_to_best_size(self):
        o_stringutils = StringUtils()

        s_best_size = o_stringutils.convert_bytes_to_best_size(100)
        assert s_best_size == "100Bytes"

        s_best_size = o_stringutils.convert_bytes_to_best_size(100*1024)
        assert s_best_size == "100.00KB"

        s_best_size = o_stringutils.convert_bytes_to_best_size(111*1024+111)
        assert s_best_size == "111.11KB"

        s_best_size = o_stringutils.convert_bytes_to_best_size(100*1024*1024)
        assert s_best_size == "100.00MB"

        s_best_size = o_stringutils.convert_bytes_to_best_size(100*1024*1024*1024)
        assert s_best_size == "100.00GB"

        s_best_size = o_stringutils.convert_bytes_to_best_size(100*1024*1024*1024*1024)
        assert s_best_size == "100.00TB"

        s_best_size = o_stringutils.convert_bytes_to_best_size(100*1024*1024*1024*1024*1024)
        assert s_best_size == "100.00PB"

    def test_convert_to_multiple_units(self):
        # , s_amount, b_add_units=False, i_decimals=2, b_remove_decimals_if_ge_1000=True
        o_stringutils = StringUtils()

        # Case 0 Bytes
        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = \
            o_stringutils.convert_to_multiple_units(s_amount="0", b_add_units=True, i_decimals=2, b_remove_decimals_if_ge_1000=True)
        assert s_value_bytes == "0Bytes"
        assert s_value_kb == "0.00KB"
        assert s_value_mb == "0.00MB"
        assert s_value_gb == "0.00GB"
        assert s_value_tb == "0.00TB"
        assert s_value_pb == "0.00PB"
        assert s_biggest_suggested == s_value_bytes

        # Case 1 Byte
        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = \
            o_stringutils.convert_to_multiple_units(s_amount="1", b_add_units=True, i_decimals=2, b_remove_decimals_if_ge_1000=True)
        assert s_value_bytes == "1Byte"
        assert s_value_kb == "0.00KB"
        assert s_value_mb == "0.00MB"
        assert s_value_gb == "0.00GB"
        assert s_value_tb == "0.00TB"
        assert s_value_pb == "0.00PB"
        assert s_biggest_suggested == s_value_bytes

        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = \
            o_stringutils.convert_to_multiple_units(s_amount="1024", b_add_units=True, i_decimals=2, b_remove_decimals_if_ge_1000=True)
        assert s_value_bytes == "1024Bytes"
        assert s_value_kb == "1.00KB"
        assert s_value_mb == "0.00MB"
        assert s_value_gb == "0.00GB"
        assert s_value_tb == "0.00TB"
        assert s_value_pb == "0.00PB"
        assert s_biggest_suggested == s_value_kb

        # Test > 1000.00 show as 1000
        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = \
            o_stringutils.convert_to_multiple_units(s_amount="1048576", b_add_units=True, i_decimals=2, b_remove_decimals_if_ge_1000=True)
        assert s_value_bytes == "1048576Bytes"
        assert s_value_kb == "1024KB"
        assert s_value_mb == "1.00MB"
        assert s_value_gb == "0.00GB"
        assert s_value_tb == "0.00TB"
        assert s_value_pb == "0.00PB"
        assert s_biggest_suggested == s_value_mb

        # 1024*1024*1024*1024*1024 = 1125899906842624
        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = \
            o_stringutils.convert_to_multiple_units(s_amount="1125899906842624", b_add_units=True, i_decimals=2, b_remove_decimals_if_ge_1000=True)
        assert s_value_bytes == "1125899906842624Bytes"
        assert s_value_kb == "1099511627776KB"
        assert s_value_mb == "1073741824MB"
        assert s_value_gb == "1048576GB"
        assert s_value_tb == "1024TB"
        assert s_value_pb == "1.00PB"
        assert s_biggest_suggested == s_value_pb

        # 1024*1024*1024*1024*1024 = 1125899906842624 and we add 101
        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = \
            o_stringutils.convert_to_multiple_units(s_amount="1125899906842725", b_add_units=True, i_decimals=2, b_remove_decimals_if_ge_1000=False)
        assert s_value_bytes == "1125899906842725Bytes"
        assert s_value_kb == "1099511627776.10KB"
        assert s_value_mb == "1073741824.00MB"
        assert s_value_gb == "1048576.00GB"
        assert s_value_tb == "1024.00TB"
        assert s_value_pb == "1.00PB"
        assert s_biggest_suggested == s_value_pb

        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = \
            o_stringutils.convert_to_multiple_units(s_amount="1125899906842725K", b_add_units=True, i_decimals=2, b_remove_decimals_if_ge_1000=True)
        assert s_value_bytes == "1152921504606950400Bytes"
        assert s_value_kb == "1125899906842725KB"
        assert s_value_mb == "1099511627776MB"
        assert s_value_gb == "1073741824GB"
        assert s_value_tb == "1048576TB"
        assert s_value_pb == "1024PB"
        assert s_biggest_suggested == s_value_pb


        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = \
            o_stringutils.convert_to_multiple_units(s_amount="1KB", b_add_units=True, i_decimals=2, b_remove_decimals_if_ge_1000=True)
        assert s_value_bytes == "1024Bytes"
        assert s_value_kb == "1.00KB"
        assert s_value_mb == "0.00MB"
        assert s_value_gb == "0.00GB"
        assert s_value_tb == "0.00TB"
        assert s_value_pb == "0.00PB"
        assert s_biggest_suggested == s_value_kb

        # Test KIB
        s_value_bytes, s_value_kb, s_value_mb, s_value_gb, s_value_tb, s_value_pb, s_biggest_suggested = \
            o_stringutils.convert_to_multiple_units(s_amount="1KIB", b_add_units=True, i_decimals=2, b_remove_decimals_if_ge_1000=True)
        assert s_value_bytes == "1024Bytes"
        assert s_value_kb == "1.00KB"
        assert s_value_mb == "0.00MB"
        assert s_value_gb == "0.00GB"
        assert s_value_tb == "0.00TB"
        assert s_value_pb == "0.00PB"
        assert s_biggest_suggested == s_value_kb

    def test_format_float_to_string(self):
        o_stringutils = StringUtils()

        s_number = o_stringutils.format_float_to_string(11.11, i_decimal_positions=2)
        assert s_number == "11.11"

        s_number = o_stringutils.format_float_to_string(11.111, i_decimal_positions=2)
        assert s_number == "11.11"

        s_number = o_stringutils.format_float_to_string(11.111, i_decimal_positions=3)
        assert s_number == "11.111"

        s_number = o_stringutils.format_float_to_string(11.11, i_decimal_positions=3)
        assert s_number == "11.110"

    def test_add_trailing_slash(self):

        o_stringutils = StringUtils()

        s_path = o_stringutils.add_trailing_slash("/home/carles")
        assert s_path == "/home/carles/"

        s_path = o_stringutils.add_trailing_slash("")
        assert s_path == "/"

        s_path = o_stringutils.add_trailing_slash("/")
        assert s_path == "/"

    def test_format_string_to_fixed_length(self):

        o_stringutils = StringUtils()

        s_text = o_stringutils.format_string_to_fixed_length(s_text="Hello World!", i_length=10, s_align_mode="L", b_truncate_excess=True)
        assert s_text == "Hello Worl"

        s_text = o_stringutils.format_string_to_fixed_length(s_text="Hello World!", i_length=10, s_align_mode="L", b_truncate_excess=False)
        assert s_text == "Hello World!"

        s_text = o_stringutils.format_string_to_fixed_length(s_text="Hello World!", i_length=20, s_align_mode="L", b_truncate_excess=True)
        assert s_text == "Hello World!        "

        s_text = o_stringutils.format_string_to_fixed_length(s_text="Hello World!", i_length=20, s_align_mode="R", b_truncate_excess=True)
        assert s_text == "        Hello World!"

    def test_html_escape(self):

        o_stringutils = StringUtils()

        s_text = "<Request & 'Test'>" + '"Test"'

        s_escaped = o_stringutils.html_escape(s_text=s_text, b_quote=True)
        assert s_escaped == "&lt;Request &amp; &#x27;Test&#x27;&gt;&quot;Test&quot;"

        s_escaped = o_stringutils.html_escape(s_text=s_text, b_quote=False)
        assert s_escaped == "&lt;Request &amp; 'Test'&gt;\"Test\""

    def test_get_dict_value(self):

        o_stringutils = StringUtils()

        d_dict_for_the_test = {"exists": "yes"}

        b_found, s_value = o_stringutils.get_dict_value(s_key="exists", d_dict=d_dict_for_the_test, m_default="")
        assert b_found is True
        assert s_value == "yes"

        b_found, s_value = o_stringutils.get_dict_value(s_key="exists", d_dict=d_dict_for_the_test, m_default="default")
        assert b_found is True
        assert s_value == "yes"

        b_found, s_value = o_stringutils.get_dict_value(s_key="does-not-exists", d_dict=d_dict_for_the_test, m_default="")
        assert b_found is False
        assert s_value == ""

        b_found, s_value = o_stringutils.get_dict_value(s_key="does-not-exists-too", d_dict=d_dict_for_the_test, m_default="default")
        assert b_found is False
        assert s_value == "default"

    def test_get_bytes_per_second(self):

        o_stringutils = StringUtils()

        i_bytes_sec, s_units_sec = o_stringutils.get_bytes_per_second(1000, 2)
        assert i_bytes_sec == 500
        assert s_units_sec == "500Bytes"

        i_bytes_sec, s_units_sec = o_stringutils.get_bytes_per_second(1024, 2)
        assert i_bytes_sec == 512
        assert s_units_sec == "512Bytes"

        i_bytes_sec, s_units_sec = o_stringutils.get_bytes_per_second(2048, 2)
        assert i_bytes_sec == 1024
        assert s_units_sec == "1.00KB"

        i_bytes_sec, s_units_sec = o_stringutils.get_bytes_per_second(2049, 2)
        assert i_bytes_sec == 1024
        assert s_units_sec == "1.00KB"

        i_bytes_sec, s_units_sec = o_stringutils.get_bytes_per_second(20480, 2)
        assert i_bytes_sec == 10240
        assert s_units_sec == "10.00KB"

        i_bytes_sec, s_units_sec = o_stringutils.get_bytes_per_second(2048*1000, 2)
        assert i_bytes_sec == 1024000
        assert s_units_sec == "1000KB"

        i_bytes_sec, s_units_sec = o_stringutils.get_bytes_per_second(2*1024*1024, 2)
        assert i_bytes_sec == 1048576
        assert s_units_sec == "1.00MB"

    def test_get_bytes_per_second_zero_seconds(self):
        o_stringutils = StringUtils()

        i_bytes_sec, s_units_sec = o_stringutils.get_bytes_per_second(1000, 0)
        assert i_bytes_sec == 1000
        assert s_units_sec == "1000Bytes"

    def test_get_percent(self):

        o_stringutils = StringUtils()

        f_percent_free, i_percent_free, s_percent_decimals, s_percent_no_decimals = o_stringutils.get_percent(i_partial=10,
                                                                                                              i_total=100,
                                                                                                              b_add_percent=True)

        assert f_percent_free == 10
        assert i_percent_free == 10
        assert s_percent_decimals == "10.0%"
        assert s_percent_no_decimals == "10%"

        f_percent_free, i_percent_free, s_percent_decimals, s_percent_no_decimals = o_stringutils.get_percent(i_partial=11,
                                                                                                              i_total=33,
                                                                                                              b_add_percent=True)
        # 33.33333333..6
        assert f_percent_free > 33.33 and f_percent_free < 33.34
        assert i_percent_free == 33
        assert s_percent_decimals == "33.33%"
        assert s_percent_no_decimals == "33%"

        f_percent_free, i_percent_free, s_percent_decimals, s_percent_no_decimals = o_stringutils.get_percent(i_partial=0,
                                                                                                              i_total=0,
                                                                                                              b_add_percent=True)

        assert f_percent_free == 0
        assert i_percent_free == 0
        assert s_percent_decimals == "100%"
        assert s_percent_no_decimals == "100%"

    def test_get_time_and_bytes_per_second(self):

        o_stringutils = StringUtils()

        f_time, s_time, i_bytes_per_second, s_best_unit = o_stringutils.get_time_and_bytes_per_second(i_bytes=1000,
                                                                                                      f_time_start=1643982635.0,
                                                                                                      f_time_finish=1643982645.1)

        assert f_time > 10.0 and f_time <= 10.1
        assert s_time == "10.1"
        assert i_bytes_per_second == 99
        assert s_best_unit == "99Bytes"

        f_time, s_time, i_bytes_per_second, s_best_unit = o_stringutils.get_time_and_bytes_per_second(i_bytes=1000000,
                                                                                                      f_time_start=1643982635.0,
                                                                                                      f_time_finish=1643982645.1)

        assert f_time > 10.0 and f_time <= 10.1
        assert s_time == "10.1"
        assert i_bytes_per_second == 99009
        assert s_best_unit == "96.69KB"
