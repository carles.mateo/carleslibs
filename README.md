# carleslibs

Carles Mateo's Python Libraries v. 1.0.8

https://blog.carlesmateo.com/carleslibs

Install in Ubuntu with:

```pip3 install carleslibs```

It is shipped with a sample demo application to easily see how it works:

```carleslibs_demo.py```

**Packgades available:**

DateTimeUtils()

FileUtils()

HashUtils()

KeyboardUtils()

MenuUtils()

OsUtils()

PythonUtils()

StringUtils()

SubProcessUtils()

